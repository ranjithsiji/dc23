# DebConf 23 Artwork
- **Guiding principle**: Our design should be humane, reliable (be consistent), expressive and clear (little to no space for ambiguity).
- Design guidelines: [Penpot Link](https://design.penpot.app/#/view/e454fca8-d3da-80ae-8002-037347793abf?page-id=3209b940-efb1-11eb-836e-355af247e295&section=interactions&index=0&share-id=66c3210d-ba6d-813c-8002-0dce6516e948)

- Base deliverables list: [Cryptpad link](https://cryptpad.fr/pad/#/2/pad/view/XFSJkTAmWrvjeRFcV2J6a3iO7QR6KmQG+qwztZDBbXE/)

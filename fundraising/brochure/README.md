Install XeTeX and dejavu, manjari fonts

# apt install texlive-xetex texlive-fonts-extra fonts-smc-manjari

Generate the pdf

$ ./make-brochure

Once the documents are final, it should be added to
https://salsa.debian.org/debconf-team/public/websites/media

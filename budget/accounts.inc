account assets:SPI
account assets:debian-ch
account assets:debian-fr
account assets:foss-united
account assets:stripe
account assets:abhijithpa

account expenses:covid:N95_mask
account expenses:covid:sanitizers
account expenses:covid:testing_kit
account expenses:equipment:leased_line
account expenses:equipment:a/v_and_network:rental
account expenses:equipment:a/v_and_network:purchase
account expenses:equipment:import_tax
account expenses:fees:SPI
account expenses:fees:bank
account expenses:fees:stripe
account expenses:food:debcamp
account expenses:food:debconf
account expenses:food:refreshments
account expenses:general
account expenses:graphic materials
account expenses:reimbursable:sponsor imports
account expenses:incidentals
account expenses:incidentals:stationary
account expenses:incidentals:rest
account expenses:local_team
account expenses:misc:childcare
account expenses:misc:printing
account expenses:misc:tea_bar
account expenses:party:cheese and wine
account expenses:party:conference dinner:bus
account expenses:party:conference dinner:food
account expenses:party:conference dinner:music
account expenses:party:social event:drinks
account expenses:party:social event:food
account expenses:promotion
account expenses:refunds
account expenses:social:after_party(barbecue)
account expenses:social:cheese_n_wine:liquor_license
account expenses:social:cheese_n_wine:purchases
account expenses:social:conference_dinner:venue
account expenses:social:conference_dinner:travel
account expenses:social:conference_dinner:catering
account expenses:social:day trip
account expenses:social:day trip:bus
account expenses:swag:merchandise
account expenses:travel:bursary:diversity
account expenses:travel:bursary:general
account expenses:travel:team:dc24
account expenses:travel:team:debconf-ctte
account expenses:travel:team:registration
account expenses:travel:team:video
account expenses:venue:debcamp:food_accomodation
account expenses:venue:debcamp:halls
account expenses:venue:debconf:food_accomodation
account expenses:venue:debconf:halls
account expenses:venue:debconf:lawn_roof
account expenses:venue:hygiene
account expenses:video:import_tax

account income:accommodation
account income:hacklab-bar
account income:meals
account income:registration
account income:sponsors:bronze
account income:sponsors:donations
account income:sponsors:gold
account income:sponsors:platinum
account income:sponsors:silver
account income:sponsors:supporter

account liabilities:abhijithpa
account liabilities:anupa
account liabilities:kiran
account liabilities:olasd
account liabilities:praveen
account liabilities:raju
account liabilities:sgk
account liabilities:sruthi
account liabilities:stefanor
account liabilities:utkarsh

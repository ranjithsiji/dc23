#!/usr/bin/python3

from argparse import ArgumentParser
from decimal import Decimal
from subprocess import check_output
import re


def read_accounts(ledger, currency):
    output = check_output(("ledger", "-f", ledger, "balance", "--flat",
                           "--empty", "--no-total", "--explicit", "--pedantic",
                           "--wide", "--price-db", "forex.db",
                           "--exchange", currency, "income", "expenses"),
                          encoding='UTF-8')
    currency_re = re.compile(r'^([A-Z]{3}) (-?[0-9,]+\.[0-9]+)$')
    accounts = {}
    for line in output.splitlines():
        line = line.strip()
        amount, account = line.split('  ')
        if amount == '0':
            accounts[account] = Decimal(amount)
        else:
            m = currency_re.match(amount)
            assert m.group(1) == currency
            amount = m.group(2).replace(',', '')
            accounts[account] = Decimal(amount)
    return accounts


def find_parent_in_budget(account, budget):
    while ':' in account:
        account = account.rsplit(':', 1)[0]
        if account in budget:
            return account
    return None


def compare(spent, budget, currency):
    # Find things that were budgeted at a higher level and move the spending up
    aggregates = {}
    moved_up = set()
    for account in spent:
        if account in budget:
            continue
        parent = find_parent_in_budget(account, budget)
        if not parent:
            continue
        aggregates.setdefault(parent, Decimal(0))
        aggregates[parent] += spent[account]
        moved_up.add(account)

    spent = spent.copy()
    spent.update(aggregates)
    for key in moved_up:
        del spent[key]

    print(f"{'Account:':44s} Currency: {'Spent:':>10s} {'Budgeted:':>10s} "
          f"{'Over:':>10s}")

    for account in sorted(spent):
        amount_spent = spent[account]
        amount_budgeted = budget.get(account, Decimal(0))
        over = amount_spent - amount_budgeted
        line = (f"{account:50s} {currency} {amount_spent:10.2f} "
                f"{amount_budgeted:10.2f}")
        if over > 0:
            line += f" {over:10.2f}"
        print(line)


def main():
    p = ArgumentParser("Compare current expenses to budgeted expenses")
    p.add_argument("--exchange", "-X", metavar="CURRENCY", type=str,
                   default="USD",
                   help="Report in the specified currency")
    p.add_argument("--forecast", "-f", action="store_true",
                   help="Include forecast payables")
    args = p.parse_args()

    if args.forecast:
        spent = read_accounts("forecast.ledger", args.exchange)
    else:
        spent = read_accounts("journal.ledger", args.exchange)
    budget = read_accounts("budget.ledger", args.exchange)
    compare(spent, budget, args.exchange)


if __name__ == '__main__':
    main()
